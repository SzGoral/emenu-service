#!/bin/sh
set -e

while ! pg_isready -h ${DATABASE_URL} -p ${DATABASE_PORT}; do
  echo "Connecting to ${DATABASE_URL} Failed. Waiting"
  sleep 5
done

echo "DB is up - proceed"

if [ "$DJANGO_MANAGEPY_MIGRATE" = 'yes' ]; then
    python manage.py migrate --noinput
fi

exec "$@"