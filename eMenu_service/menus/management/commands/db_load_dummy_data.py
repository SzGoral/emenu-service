import json
import os

from pathlib import Path
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.db import IntegrityError
from menus.models import Menu, Dish


BASE_DIR = Path(__file__).resolve().parents[3]


class Command(BaseCommand):
    help = 'Notify users about news in menu'

    def handle(self, *args, **kwargs):
        dummy_data = os.path.join(BASE_DIR, 'db_dummy_data.json')
        with open(dummy_data, 'r') as file:
            data = json.load(file)

        for user, params in data['users'].items():
            try:
                User.objects.create_user(user, params['email'], params['password'])
            except IntegrityError:
                pass

        for menu, params in data['menus'].items():
            try:
                Menu.objects.create(
                    name=menu,
                    description=params['description']
                )
            except IntegrityError:
                pass

        for dish, params in data['dishes'].items():
            try:
                Dish.objects.get(name=dish)
            except:
                dish = Dish.objects.create(
                    name=dish,
                    description=params['description'],
                    price=params['price'],
                    prepare_time=params['prepare_time'],
                    is_vegetarian=params['is_vegetarian'],
                )
                in_menu = params['available_in_menu']
                for name in in_menu:
                    menu = Menu.objects.get(name=name)
                    dish.available_in_menu.add(menu)
