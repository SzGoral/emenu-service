import datetime

from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.core.management.base import BaseCommand
from menus.models import Dish


class Command(BaseCommand):
    help = 'Notify users about news in menu'

    def handle(self, *args, **kwargs):
        emails = User.objects.filter(is_active=True).exclude(email='').values_list('email', flat=True)
        yesterday = datetime.date.today() - datetime.timedelta(days=1)
        new_dishes = Dish.objects.filter(add_date__date=yesterday)
        updated_dishes = Dish.objects.filter(update_date__date=yesterday)
        if new_dishes:
            all_new = ''
            for dish in new_dishes:
                all_new += f"{dish}\n"
            message = f"Yesterday {len(new_dishes)} menu(s) was created!\n" \
                      f"New dishes:\n{all_new}"
            self.notify_users(emails, msg=message, title="Dishes Created")

        if updated_dishes:
            all_updated = ''
            for dish in updated_dishes:
                all_updated += f"{dish}\n"
            message = f"Yesterday {len(new_dishes)} menu(s) was updated!\n" \
                      f"Updated dishes:\n{all_updated}"
            self.notify_users(emails, msg=message, title="Dishes Updated")

    @staticmethod
    def notify_users(emails, msg='', title='') -> None:
        for email in emails:
            send_mail(title, msg, '', [email], fail_silently=False)
