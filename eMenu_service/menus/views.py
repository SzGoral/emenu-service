from django.db.models import Count

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Menu, Dish
from .serializers import MenuSerializer, DishSerializer, MenuDetailSerializer


@api_view(['GET'])
def api_overview(request):
    """
    all endpoints
    """
    if request.user.is_authenticated:
        api_urls = {
            'Public List [available parameters: '
            '?sort_by=name, ?sort_by=dishes]': '/menu-list',
            'Public Detail View': '/menu-detail/<str:pk>/',
            'Private List': '/menu-private_list',
            'Private Detail View': '/menu-private_detail/<str:pk>/',
            'Create': '/menu-create',
            'Update': '/menu-update/<str:pk>',
            'Delete': '/menu-delete/<str:pk>',
            'Dish List': '/dish-list',
            'Dish Detail View': '/dish-detail/<str:pk>/',
            'Dish Create': '/dish-create',
            'Dish Update': '/dish-update/<str:pk>',
            'Dish Delete': '/dish-delete/<str:pk>',
        }
    else:
        api_urls = {
            'List': '/menu-list',
            'Detail View': '/menu-detail/<str:pk>/',
        }
    return Response(api_urls)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def menu_private_list(request):
    """
    get list of all menus - available only for logged users
    """
    menus = Menu.objects.all()
    serializer = MenuSerializer(menus, many=True)
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def menu_private_detail(request, pk):
    """
    get detailed info of specific menu based on pk - available only for logged users
    """
    try:
        menus = Menu.objects.get(id=pk)
    except Menu.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    serializer = MenuSerializer(menus, many=False)
    return Response(serializer.data)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def menu_create(request):
    """
    create new menu - available only for logged users
    """
    serializer = MenuSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response({'success': 'create successful'})
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def menu_update(request, pk):
    """
    update existing menu - available only for logged users
    """
    try:
        menus = Menu.objects.get(id=pk)
    except Menu.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    else:
        serializer = MenuSerializer(instance=menus, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'success': 'update successful'})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def menu_delete(request, pk):
    """
    delete existing menu - available only for logged users
    """
    try:
        menus = Menu.objects.get(id=pk)
    except Menu.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    else:
        menus.delete()
        return Response({'success': 'delete successful'})


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def dish_list(request):
    """
    get list of all dishes - available only for logged users
    """
    dishes = Dish.objects.all()
    serializer = DishSerializer(dishes, many=True)
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def dish_detail(request, pk):
    """
    get detailed info of specific dish based on pk - available only for logged users
    """
    try:
        dish = Dish.objects.get(id=pk)
    except Dish.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    serializer = DishSerializer(dish, many=False)
    return Response(serializer.data)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def dish_create(request):
    """
    create new dish - available only for logged users
    """
    serializer = DishSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response({'success': 'create successful'})
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def dish_update(request, pk):
    """
    update existing dish - available only for logged users
    to add dish to menu use:
    {
        "available_in_menu": ["<menu1-name>", "<menu2-name>", ...]
    }
    """
    try:
        dish = Dish.objects.get(id=pk)
    except Dish.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    else:
        serializer = DishSerializer(instance=dish, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'success': 'update successful'})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def dish_delete(request, pk):
    """
    delete existing dish - available only for logged users
    """
    try:
        dish = Dish.objects.get(id=pk)
    except Dish.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    else:
        dish.delete()
        return Response({'success': 'delete successful'})


@api_view(['GET'])
def menu_list(request):
    """
    get list of all non empty menus - public access
    available parameters:
        1. ?sort_by=name -> sort by menu name
        2. ?sort_by=dishes -> sort by no. of dishes in menu
    """
    sort_by = request.GET.get('sort_by')
    if sort_by == 'name':
        menus = Menu.objects.order_by('name')
    elif sort_by == 'dishes':
        menus = Menu.objects.annotate(dishes_no=Count('dishes')).order_by('-dishes_no')
    else:
        menus = Menu.objects.all()
    to_display = []
    for menu in menus:
        if menu.dishes.all():
            to_display.append(menu)
    if to_display:
        serializer = MenuSerializer(to_display, many=True)
        return Response(serializer.data)
    else:
        return Response({'info': 'there is no menu to display.'})


@api_view(['GET'])
def menu_detail(request, pk):
    """
    get detailed info of specific, non empty, menu based on pk - public access
    """
    try:
        menu = Menu.objects.get(id=pk)
    except Menu.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if menu.dishes.all():
        serializer = MenuDetailSerializer(menu, many=False)
        return Response(serializer.data)
    else:
        return Response({'info': 'there are no dishes in this menu.'})
