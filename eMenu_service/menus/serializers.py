from rest_framework import serializers

from .models import Menu, Dish


class DishSerializer(serializers.ModelSerializer):
    available_in_menu = serializers.SlugRelatedField(
        many=True,
        slug_field='name',
        queryset=Menu.objects.all(),
        required=False
     )

    class Meta:
        model = Dish
        fields = ['id', 'name', 'description', 'price', 'prepare_time',
                  'add_date', 'update_date', 'is_vegetarian', 'available_in_menu']
        extra_kwargs = {'available_in_menu': {'required': False}}


class MenuDetailSerializer(serializers.ModelSerializer):
    dishes = DishSerializer(many=True)

    class Meta:
        model = Menu
        fields = ['id', 'name', 'description', 'add_date', 'update_date', 'dishes']
        extra_kwargs = {'dishes': {'required': False}}


class MenuSerializer(serializers.ModelSerializer):
    dishes = serializers.SlugRelatedField(
        many=True,
        slug_field='name',
        queryset=Dish.objects.all(),
        required=False
     )

    class Meta:
        model = Menu
        fields = ['id', 'name', 'description', 'add_date', 'update_date', 'dishes']

