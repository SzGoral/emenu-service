from django.db import models
from django.utils import timezone


class Menu(models.Model):
    name = models.CharField(max_length=50, unique=True, blank=True)
    description = models.TextField()
    add_date = models.DateTimeField(default=timezone.now)
    update_date = models.DateTimeField(blank=True, null=True, auto_now=True)

    objects = models.Manager()

    def __str__(self):
        return self.name


class Dish(models.Model):
    name = models.CharField(max_length=50, blank=True)
    description = models.TextField(blank=True)
    price = models.FloatField(blank=True)
    prepare_time = models.TimeField(blank=True)
    add_date = models.DateTimeField(default=timezone.now)
    update_date = models.DateTimeField(blank=True, null=True, auto_now=True)
    is_vegetarian = models.BooleanField(blank=True)
    image = models.ImageField(default='default.png', upload_to='dishes_pics')
    available_in_menu = models.ManyToManyField(Menu, related_name='dishes', blank=True)

    objects = models.Manager()

    def __str__(self):
        return self.name
