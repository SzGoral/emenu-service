from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('', views.api_overview, name='api-overview'),
    path('menu-private_list/', views.menu_private_list, name='menu-private_list'),
    path('menu-private_detail/<str:pk>', views.menu_private_detail, name='menu-private_detail'),
    path('menu-create/', views.menu_create, name='menu-create'),
    path('menu-update/<str:pk>', views.menu_update, name='menu-update'),
    path('menu-delete/<str:pk>', views.menu_delete, name='menu-delete'),

    path('dish-list/', views.dish_list, name='dish-list'),
    path('dish-detail/<str:pk>', views.dish_detail, name='dish-detail'),
    path('dish-create/', views.dish_create, name='dish-create'),
    path('dish-update/<str:pk>', views.dish_update, name='dish-update'),
    path('dish-delete/<str:pk>', views.dish_delete, name='dish-delete'),

    path('menu-list/', views.menu_list, name='menu-list'),
    path('menu-detail/<str:pk>', views.menu_detail, name='menu-detail'),
]

if settings.DEBUG:  # pragma: no cover
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
