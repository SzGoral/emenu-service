import json

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from .models import Menu, Dish


class ViewTestCases(APITestCase):

    def setUp(self) -> None:
        self.user = User.objects.create_user('testuser', 'test@user.com', 'testuserpwd')
        self.client.login(username='testuser', password='testuserpwd')

        self.menu1 = Menu.objects.create(
            name='test_menu_no_dishes',
            description='description of test menu without dishes',
            add_date='2021-05-04T12:30:00+02:00',
            update_date='2021-05-04T13:31:00+02:00',
        )

        self.menu2 = Menu.objects.create(
            name='test_menu_dishes',
            description='description of test menu with dishes',
            add_date='2021-05-05T12:30:00+02:00',
            update_date='2021-05-05T13:31:00+02:00',
        )

        self.dish1 = Dish.objects.create(
            name='tomato soup',
            description='soup made with tomato',
            price=12.88,
            prepare_time='00:14',
            add_date='2021-05-05T13:30:00+02:00',
            update_date='2021-05-05T13:31:10+02:00',
            is_vegetarian=True,
        )
        self.dish1.available_in_menu.add(self.menu2)

        self.dish2 = Dish.objects.create(
            name='cucumber soup',
            description='soup made with cucumber',
            price=11.11,
            prepare_time='00:16',
            add_date='2021-05-05T14:30:00+02:00',
            update_date='2021-05-05T15:31:10+02:00',
            is_vegetarian=True,
        )

    def get_non_exist_pk(self, table):
        queryobj = table.objects.order_by('-pk').first()
        non_exist_pk = queryobj.pk + 1
        return non_exist_pk

    def test_menu_list(self):
        self.client.logout()
        response = self.client.get(reverse('menu-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        response = self.client.post(reverse('menu-list'),
                                    {
                                        'name': 'new menu',
                                        'description': 'new menu'
                                    })
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_menu_detail(self):
        self.client.logout()
        response = self.client.get(reverse('menu-detail', kwargs={'pk': self.menu1.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['info'], 'there are no dishes in this menu.')

        response = self.client.put(reverse('menu-detail', kwargs={'pk': self.menu1.pk}),
                                   {
                                       'description': 'description updated'
                                   })
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.get(reverse('menu-detail', kwargs={'pk': self.menu2.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], 'test_menu_dishes')

        response = self.client.get(reverse('menu-detail', kwargs={'pk': self.get_non_exist_pk(Menu)}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_menu_private_list(self):
        response = self.client.get(reverse('menu-private_list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

        response = self.client.post(reverse('menu-list'),
                                    {
                                        'name': 'new menu',
                                        'description': 'new menu'
                                    })
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.client.logout()
        response = self.client.get(reverse('menu-private_list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_menu_private_detail(self):
        response = self.client.get(reverse('menu-private_detail', kwargs={'pk': self.menu1.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['dishes'], [])

        response = self.client.put(reverse('menu-private_detail', kwargs={'pk': self.menu1.pk}),
                                   {
                                       'description': 'description updated'
                                   })
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.get(reverse('menu-private_detail', kwargs={'pk': self.menu2.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['dishes'], ['tomato soup'])

        response = self.client.get(reverse('menu-private_detail', kwargs={'pk': self.get_non_exist_pk(Menu)}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        self.client.logout()
        response = self.client.put(reverse('menu-private_detail', kwargs={'pk': self.menu1.pk}),
                                   {'description': 'description updated'})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_menu_create(self):
        response = self.client.get(reverse('menu-create'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.post(reverse('menu-create'),
                                    {
                                        "name": "new_menu",
                                        "description": "description of new menu"
                                    }, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.post(reverse('menu-create'),
                                    {
                                        "name": "new_menu",
                                        "description": "description of new menu"
                                    }, format='json')
        self.assertEqual(*response.data['name'], 'menu with this name already exists.')

        self.client.logout()
        response = self.client.post(reverse('menu-create'),
                                    {
                                        "name": "new_menu",
                                        "description": "description of new menu"
                                    }, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.assertEqual(len(Menu.objects.all()), 3)

    def test_menu_update(self):
        response = self.client.get(reverse('menu-update', kwargs={'pk': self.menu1.pk}))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        old_desc = Menu.objects.get(id=self.menu2.pk).description
        response = self.client.put(reverse('menu-update', kwargs={'pk': self.menu2.pk}),
                                   {
                                       'description': 'new desc'
                                   }, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_desc = Menu.objects.get(id=self.menu2.pk).description
        self.assertEqual(response.data['success'], 'update successful')
        self.assertNotEqual(old_desc, new_desc)

        response = self.client.put(reverse('menu-update', kwargs={'pk': self.get_non_exist_pk(Menu)}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        self.client.logout()
        response = self.client.put(reverse('menu-update', kwargs={'pk': self.menu2.pk}),
                                   {
                                       "description": "description of new menu"
                                   }, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_menu_delete(self):
        response = self.client.delete(reverse('menu-delete', kwargs={'pk': self.menu1.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(Menu.objects.all()), 1)

        response = self.client.delete(reverse('menu-delete', kwargs={'pk': self.get_non_exist_pk(Menu)}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        self.client.logout()
        response = self.client.put(reverse('menu-delete', kwargs={'pk': self.menu2.pk}))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_dish_list(self):
        response = self.client.get(reverse('dish-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

        response = self.client.post(reverse('dish-list'),
                                    {
                                        'name': 'new dish',
                                        'description': 'new dish desc'
                                    })
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.client.logout()
        response = self.client.get(reverse('dish-list'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_dish_detail(self):
        response = self.client.get(reverse('dish-detail', kwargs={'pk': self.dish1.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['available_in_menu'], ['test_menu_dishes'])

        response = self.client.put(reverse('dish-detail', kwargs={'pk': self.dish1.pk}),
                                   {
                                       'description': 'description updated'
                                   })
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.get(reverse('dish-detail', kwargs={'pk': self.get_non_exist_pk(Dish)}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        self.client.logout()
        response = self.client.get(reverse('dish-detail', kwargs={'pk': self.dish1.pk}))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_dish_create(self):
        response = self.client.get(reverse('dish-create'))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.post(reverse('dish-create'),
                                    {
                                        'name': 'new_dish',
                                        'description': 'new dish desc',
                                        'price': 12.5,
                                        'prepare_time': '00:10',
                                        'is_vegetarian': True,
                                        'available_in_menu': [],
                                    }, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.client.logout()
        response = self.client.post(reverse('dish-create'),
                                    {
                                        'name': 'new_dish',
                                        'description': 'new dish desc',
                                        'price': 12.5,
                                        'prepare_time': '00:10',
                                        'is_vegetarian': True,
                                        'available_in_menu': [],
                                    }, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.assertEqual(len(Dish.objects.all()), 3)

    def test_dish_update(self):
        response = self.client.get(reverse('dish-update', kwargs={'pk': self.dish1.pk}))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        old_desc = Dish.objects.get(id=self.dish2.pk).description
        response = self.client.put(reverse('dish-update', kwargs={'pk': self.dish2.pk}),
                                   {
                                       'description': 'new dish desc'
                                   }, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        new_desc = Dish.objects.get(id=self.dish2.pk).description
        self.assertEqual(response.data['success'], 'update successful')
        self.assertNotEqual(old_desc, new_desc)

        response = self.client.put(reverse('dish-update', kwargs={'pk': self.dish2.pk}),
                                   {
                                       'available_in_menu': ['test_menu_dishes']
                                   }, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['success'], 'update successful')
        self.assertNotEqual(old_desc, new_desc)

        response = self.client.put(reverse('dish-update', kwargs={'pk': self.get_non_exist_pk(Dish)}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        self.client.logout()
        response = self.client.put(reverse('dish-update', kwargs={'pk': self.dish2.pk}),
                                   {
                                       "description": "new dish desc"
                                   }, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_dish_delete(self):
        response = self.client.delete(reverse('dish-delete', kwargs={'pk': self.dish1.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(Dish.objects.all()), 1)

        response = self.client.delete(reverse('dish-delete', kwargs={'pk': self.get_non_exist_pk(Dish)}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        self.client.logout()
        response = self.client.put(reverse('dish-delete', kwargs={'pk': self.dish2.pk}))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_api_overview(self):
        response = self.client.get(reverse('api-overview'))
        correct_result = {
            "Public List [available parameters: ?sort_by=name, ?sort_by=dishes]": "/menu-list",
            "Public Detail View": "/menu-detail/<str:pk>/",
            "Private List": "/menu-private_list",
            "Private Detail View": "/menu-private_detail/<str:pk>/",
            "Create": "/menu-create",
            "Update": "/menu-update/<str:pk>",
            "Delete": "/menu-delete/<str:pk>",
            "Dish List": "/dish-list",
            "Dish Detail View": "/dish-detail/<str:pk>/",
            "Dish Create": "/dish-create",
            "Dish Update": "/dish-update/<str:pk>",
            "Dish Delete": "/dish-delete/<str:pk>"
            }
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, correct_result)

        self.client.logout()
        response = self.client.get(reverse('api-overview'))
        correct_result = {
            'List': '/menu-list',
            'Detail View': '/menu-detail/<str:pk>/',
            }
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, correct_result)
