import os
import subprocess
import time
from pathlib import Path
import schedule
import threading


def thread_is_running(thread_name):
    result = False
    for thread in threading.enumerate():
        if thread.getName() == thread_name:
            result = True
    return result


def run_threaded(job_func, *args, **kwargs):
    while thread_is_running(job_func.__name__):
        return None
    job_thread = threading.Thread(
        target=job_func,
        name=job_func.__name__,
        kwargs=kwargs,
        args=args
    )
    job_thread.start()


def mail_notification():
    BASE_DIR = Path(__file__).resolve().parents[1]
    subprocess.call(['python', os.path.join(BASE_DIR, 'manage.py'), 'menus_daily_update'])


if __name__ == '__main__':
    schedule.every().day.at('10:00').do(run_threaded, mail_notification)
    while True:
        schedule.run_pending()
        time.sleep(1)
