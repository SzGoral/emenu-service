# eMenu

eMenu service -an online menu card for restaurants.

## Running application (docker)

#### note:
docker is setup to use ```PostgreSQL``` database.

#### requirements:
docker and docker-compose installed

#### setup:
pull repository and change directory to ./eMenu_service


### 1. Build api image:
```
docker-compose build
```
container name: ```emenuAPI_container```

### 2. Run container

On linux change read/write permissions for ```entrypoint.sh```
```
chmod +x entrypoint.sh
```
On windows make sure you have set: ```git config --global core.autocrlf false``` or change end-of-line sequence to ```LF``` in ```entrypoint.sh```

Next
```
docker-compose up
```

if you want to run container in the backgroud use ```-d``` flag.

Once the server is running, application is available under ```127.0.0.1:8000``` address (documentation).

api address: ```127.0.0.1:8000/api```

### 3. Initialize database with dummy data

If the container is running (```docker-compose up```) open new terminal and go into container's bash:
```
docker exec -it emenuAPI_container bash
```

Once you are in shell, execute following command:
```
python manage.py db_load_dummy_data
```

### 4. Create new user - private API verification

If database is initialized with dummy data (see ```3.```)
you can login using following credentials:
- username: ```testuser```
- password: ```strongpassword```

To create new user, go to container's bash:
```
docker exec -it emenuAPI_container bash```
```
and execute follwing command:
```
python manage.py createsuperuser
```

### 5. Daily user notofication

to send email to all users with every yesterday's create/update:
- run container (```docker-compose up```)
- open container's bash (```docker exec -it emenuAPI_container bash```) and run smtp server:
```
python -m smtpd -n -c DebuggingServer localhost:1025
```
- open second container's bash (```docker exec -it emenuAPI_container bash```) and execute:
```
python menus/cmd_scheduler.py
```

Mail will be send every day at 10:00

### 6. Dish - Available in menu

to change the menus in which specific dish should be available, use ```api/dish-update/<pk>``` and send data like this:
```
{
    "available_in_menu": ["<menu1-name", "menu2-name", ...]
}
```
